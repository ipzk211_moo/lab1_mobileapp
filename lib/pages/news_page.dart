import 'package:flutter/material.dart';
import 'package:mobile_app/components/news_item.dart';

class NewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        AppBar(
          title: Text('Новини'),
          centerTitle: true,
        ),
        NewsItem(
          imageUrl: 'assets/default_image.png',
          title: 'Title 1',
          date: 'Date 1',
          description: 'Description 1',
        ),
        NewsItem(
          imageUrl: 'assets/default_image.png',
          title: 'Title 2',
          date: 'Date 2',
          description: 'Description 2',
        ),
        NewsItem(
          imageUrl: 'assets/default_image.png',
          title: 'Title 3',
          date: 'Date 3',
          description: 'Description 3',
        ),
        NewsItem(
          imageUrl: 'assets/default_image.png',
          title: 'Title 4',
          date: 'Date 4',
          description: 'Description 4',
        ),
        NewsItem(
          imageUrl: 'assets/default_image.png',
          title: 'Title 5',
          date: 'Date 5',
          description: 'Description 5',
        ),
      ],
    );
  }
}
